import arcade
import constants


class Crosshair(arcade.Sprite):
    def __init__(self):
        super().__init__()
        self.texture = arcade.load_texture('player/Crosshair.png')
        self.scale = constants.SCALING
        self.angle = 45


class CrosshairCenter(arcade.Sprite):
    def __init__(self):
        super().__init__()
        self.texture = arcade.load_texture('player/Crosshair_Center.png')
        self.scale = constants.SCALING
        self.angle = 45
