import arcade
import time
import math
from math import sqrt
import constants
import player
import enemy
import crosshair
import random


class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self):
        """
        Initializer
        """
        super().__init__(constants.SCREEN_WIDTH, constants.SCREEN_HEIGHT, constants.SCREEN_TITLE)
        self.diaganal = None
        self.physics_engine_enemy = None
        self.follow = None
        self.path = None
        self.barrier_list = None
        # Sprite lists
        self.background = None
        self.wall_list = None
        self.level_list = None
        self.shuffle = None
        self.stationary_spawn_list = None
        self.player_list = None
        self.player = None
        self.player_arms_list = None
        self.player_arms = None
        self.enemy_list = None
        self.enemy = None
        self.play = None
        self.bullet_list = None
        self.enemy_bullet_list = None
        self.crosshair_list = None
        self.crosshair = None
        self.crosshairmid_list = None
        self.crosshairmid = None
        self.cross_x = None
        self.cross_y = None
        self.mouse_x = 0
        self.mouse_y = 0
        self.true_x = 0

        # Set up the player
        self.score = 0

        self.A = False
        self.D = False

        self.physics_engine = None
        self.view_left = 0
        self.view_bottom = 0
        self.end_of_map = 0
        self.last_time = None
        self.frame_count = 0
        self.fps_message = None

        self.level = 1
        self.max_level = 7

    def setup(self):
        """ Set up the game and initialize the variables. """
        self.background = arcade.load_texture('Glow Caves/Glow Caves Background.png')
        # Sprite lists
        self.level_list = arcade.SpriteList()
        self.wall_list = arcade.SpriteList()
        self.shuffle = []

        self.path = []

        self.player_list = arcade.SpriteList()
        self.player = player.PlayerCharacter()

        self.player.center_x = constants.SCREEN_WIDTH // 2
        self.player.center_y = constants.SCREEN_HEIGHT // 2
        self.player_list.append(self.player)

        self.player_arms_list = arcade.SpriteList()
        self.player_arms = player.PlayerHands()
        self.player_arms.center_x = self.player.center_x
        self.player_arms.center_y = self.player.center_y
        self.player_arms_list.append(self.player_arms)

        self.crosshair_list = arcade.SpriteList()
        self.crosshair = crosshair.Crosshair()
        self.crosshair.center_x = self.player.center_x
        self.crosshair.center_y = self.player.center_y
        self.crosshair_list.append(self.crosshair)
        self.cross_x = 0
        self.cross_y = 0

        self.crosshairmid_list = arcade.SpriteList()
        self.crosshairmid = crosshair.CrosshairCenter()
        self.crosshairmid.center_x = 0
        self.crosshairmid.center_y = 0
        self.crosshairmid_list.append(self.crosshairmid)
        # self.stationary_spawn_list = arcade.SpriteList()

        self.enemy_list = arcade.SpriteList()
        self.enemy = enemy.Enemy()
        self.enemy.center_x = constants.SCREEN_WIDTH // 2
        self.enemy.center_y = constants.SCREEN_HEIGHT // 2
        self.enemy_list.append(self.enemy)

        self.bullet_list = arcade.SpriteList()
        # Set up the player
        self.load_level()
        self.set_mouse_visible(False)
        self.center_window()

        grid_size = 32

        # Calculate the playing field size. We can't generate paths outside of
        # this.
        playing_field_left_boundary = 0
        playing_field_right_boundary = constants.SCREEN_WIDTH * 7
        playing_field_top_boundary = constants.SCREEN_HEIGHT + 100
        playing_field_bottom_boundary = 0

        self.barrier_list = arcade.AStarBarrierList(self.enemy,
                                                    self.level_list,
                                                    grid_size,
                                                    playing_field_left_boundary,
                                                    playing_field_right_boundary,
                                                    playing_field_bottom_boundary,
                                                    playing_field_top_boundary)
        self.enemy.barrier_list = self.barrier_list

    def load_level(self):
        # Read in the tiled map
        while len(self.shuffle) < 5:
            rand = random.randint(1, self.max_level)
            if rand in self.shuffle:
                print('repeat')
            else:
                self.shuffle.append(rand)
        random.shuffle(self.shuffle)
        print(self.shuffle)
        self.level_list.extend(arcade.tilemap.process_layer(arcade.tilemap.read_tmx('Glow Caves/glowcavesstart.tmx'),
                                                            'Ground',
                                                            constants.TILE_SPRITE_SCALING,
                                                            use_spatial_hash=True))
        for i in range(5):
            my_map = arcade.tilemap.read_tmx(f"Glow Caves/glowcaves{self.shuffle[i - 1]}.tmx")

            # --- Walls ---

            # Grab the layer of items we can't move through

            self.wall_list = arcade.tilemap.process_layer(my_map,
                                                          'Ground',
                                                          constants.TILE_SPRITE_SCALING,
                                                          use_spatial_hash=True)
            for wall in self.wall_list:
                wall.center_x += 1280 * (i + 1)

            self.level_list.extend(self.wall_list)

        """self.stationary_spawn_list = arcade.tilemap.process_layer(my_map,
                                                                  'Stationary Spawn Layer',
                                                                  constants.TILE_SPRITE_SCALING,
                                                                  use_spatial_hash=False)"""

        end_level = arcade.tilemap.process_layer(arcade.tilemap.read_tmx('Glow Caves/glowcavesend.tmx'),
                                                 'Ground',
                                                 constants.TILE_SPRITE_SCALING,
                                                 use_spatial_hash=True)
        for wall in end_level:
            wall.center_x += 1280 * 6
        self.level_list.extend(end_level)
        self.physics_engine = arcade.PhysicsEnginePlatformer(self.player,
                                                             self.level_list,
                                                             gravity_constant=constants.GRAVITY)

        self.physics_engine_enemy = arcade.PhysicsEnginePlatformer(self.enemy,
                                                                   self.level_list,
                                                                   gravity_constant=constants.GRAVITY)
        self.player_arms.wall_list = self.level_list
        self.enemy.level_list = self.level_list
        self.player.physics_engines.append(self.physics_engine)
        self.enemy.physics_engines.append(self.physics_engine_enemy)

    def on_draw(self):
        """
        Render the screen.
        """
        arcade.start_render()

        self.frame_count += 1

        # This command has to happen before we start drawing
        for i in range(7):
            arcade.draw_lrwh_rectangle_textured(constants.SCREEN_WIDTH * i, 0, constants.SCREEN_WIDTH,
                                                constants.SCREEN_HEIGHT, self.background)
        # Draw all the sprites.
        self.player_list.draw()
        self.player_arms_list.draw()
        self.player_arms.on_draw()
        self.level_list.draw()
        self.bullet_list.draw()
        self.enemy_list.draw()
        self.crosshair_list.draw()
        self.crosshairmid_list.draw()

        if self.last_time and self.frame_count % 60 == 0:
            fps = 1.0 / (time.time() - self.last_time) * 60
            self.fps_message = f"FPS: {fps:5.0f}"

        # if self.fps_message:
        # arcade.draw_text(self.fps_message, self.view_left + 10, self.view_bottom + 40, arcade.color.BLACK, 14)

        if self.frame_count % 60 == 0:
            self.last_time = time.time()
        if self.diaganal:
            if self.path:
                arcade.draw_line_strip(self.path, arcade.color.RED, 5)
            if self.frame_count % 60 == 0 and self.path:
                print(self.path)
                print(f'Points in path: {len(self.path)}')
            if self.fps_message:
                arcade.draw_text(self.fps_message, self.view_left, constants.SCREEN_HEIGHT - 14, (0, 255, 0))
            if self.player_arms.magazine > 0:
                arcade.draw_text(f'{self.player_arms.magazine}/30', self.player.center_x - 15,
                                 self.player.center_y + 30, (255, 0, 0))
            else:
                arcade.draw_text('RELOAD', self.player.center_x - 20, self.player.center_y + 30, (255, 0, 0))

        # Put the text on the screen.
        # Adjust the text position based on the view port so that we don't
        # scroll the text too.
        # arcade.draw_text(output, self.view_left + 10, self.view_bottom + 20, arcade.color.BLACK, 14)

    def on_key_press(self, key, modifiers):
        self.player.on_key_press(key)
        self.player_arms.on_key_press(key)
        self.enemy.on_key_press(key)
        if key == arcade.key.QUOTELEFT:
            if self.diaganal:
                self.diaganal = False
            elif not self.diaganal:
                self.diaganal = True

    def on_key_release(self, key, modifiers):
        self.player.on_key_release(key)
        self.enemy.on_key_release(key)

    def on_mouse_press(self, x, y, button, modifiers):
        """ Called whenever the mouse button is clicked. """
        self.player_arms.on_mouse_press()

    def on_mouse_release(self, x, y, button, modifiter):
        self.player_arms.on_mouse_release()

    def on_update(self, delta_time):
        """ Movement and game logic """
        self.player.physics_engines[0].update()
        self.physics_engine_enemy.update()
        enemypos = (int(self.enemy.center_x), int(self.enemy.center_y+27))
        playerpos = (int(self.player.center_x+13.5), int(self.player.center_y + 27))
        self.path = arcade.astar_calculate_path(enemypos,
                                                playerpos,
                                                self.barrier_list,
                                                diagonal_movement=False)

        self.enemy.path = self.path

        self.move_crosshair()

        self.view_left = (self.player.center_x - constants.SCREEN_WIDTH + self.crosshair.center_x) / 2
        self.view_left = int(self.view_left)
        self.view_bottom = int(self.view_bottom)
        arcade.set_viewport(self.view_left,
                            constants.SCREEN_WIDTH + self.view_left,
                            self.view_bottom,
                            constants.SCREEN_HEIGHT + self.view_bottom)

        if self.player.change_x != 0:
            self.player_arms.animate = True
        else:
            self.player_arms.animate = False
        self.enemy_list.update()
        self.player_list.update()
        self.player_list.update_animation()
        self.player_arms.update()
        self.player_arms_list.update_animation()
        self.player_arms.center_x = self.player.center_x
        self.player_arms.center_y = self.player.center_y + 7

        # Position the start at the enemy's current location
        start_x = self.player_arms.center_x
        start_y = self.player_arms.center_y

        # Get the destination location for the bullet
        dest_x = self.crosshair.center_x
        dest_y = self.crosshair.center_y

        x_diff = dest_x - start_x
        y_diff = dest_y - start_y
        angle = math.atan2(y_diff, x_diff)

        # Set the enemy to face the player.
        self.player_arms.angle = math.degrees(angle)

        if 1.6 >= angle >= -1.6:
            self.player_arms.FACING = 0
            self.player.FACING = 0

        else:
            self.player_arms.FACING = 1
            self.player.FACING = 1

    def on_mouse_motion(self, x, y, delta_x, delta_y):
        """Called whenever the mouse moves. """
        self.mouse_x = x
        self.mouse_y = y

    def move_crosshair(self):
        """Makes the crosshair float behind the curser"""
        self.true_x = self.mouse_x - constants.SCREEN_WIDTH / 2
        self.cross_x = self.mouse_x + self.view_left
        self.cross_y = self.mouse_y
        self.crosshairmid.center_x = self.mouse_x + self.view_left
        self.crosshairmid.center_y = self.mouse_y

        x_dist = self.cross_x - self.crosshair.center_x
        y_dist = self.cross_y - self.crosshair.center_y

        distance = sqrt(x_dist * x_dist + y_dist * y_dist)

        if distance > 1:
            if arcade.get_distance(self.player.center_x, self.player.center_y, self.crosshairmid.center_x,
                                   self.crosshairmid.center_y) < constants.ZOOM_AMMOUNT:
                self.crosshair.center_x += x_dist * 0.2
                self.crosshair.center_y += y_dist * 0.2

            else:
                angle = math.atan2((self.crosshairmid.center_y - self.player.center_y),
                                   (self.crosshairmid.center_x - self.player.center_x))
                x_dist = math.cos(angle) * constants.ZOOM_AMMOUNT + self.player.center_x - self.crosshair.center_x
                y_dist = math.sin(angle) * constants.ZOOM_AMMOUNT + self.player.center_y - self.crosshair.center_y
                distance = sqrt(x_dist * x_dist + y_dist * y_dist)
                if distance > 1:
                    self.crosshair.center_x += x_dist * 0.2
                    self.crosshair.center_y += y_dist * 0.2
                else:
                    self.crosshair.center_x = math.cos(angle) * constants.ZOOM_AMMOUNT + self.player.center_x
                    self.crosshair.center_y = math.sin(angle) * constants.ZOOM_AMMOUNT + self.player.center_y

            self.player_arms.aim_x = self.crosshair.center_x
            self.player_arms.aim_y = self.crosshair.center_y


def main():
    window = MyGame()
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()
