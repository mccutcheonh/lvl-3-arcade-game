import arcade

OPEN = []  # the set of nodes to be evaluated
CLOSED = []  # the set of nodes already evaluated

# add start "node" to the open list

"""OPEN.append(enemy.position)

loop:
    current = # node in OPEN with the lowest f_cost
    OPEN.remove(current)  # remove current from OPEN
    CLOSED.append(current)# add current to CLOSED

    if current == target # if current is the target node // path has been found
        break # exit the loop as the end has been found

    for neighbour in current:  # foreach neighbour of the current node
        if neighbour != traversable or neighbour in CLOSED:  # if neighbour is not traversable or neighbour is in CLOSED
            #i dont know what to put here  # skip to next neighbour
        pass

        # if new path to neighbour is shorter OR neighbour is not in OPEN
            # set f_cost of neighbour
            # set parent of neighbour to current
            # if neighbour is not in OPEN
                # add neighbout to OPEN
"""

# startPos = enemy.position
# targetPos = player.position
startPos = (200, 200)
targetPos = (0, 0)
gCost = 0
hCost = 0

fCost = (gCost + hCost)


def FindPath():
    i = 1
    startnode = startPos
    endnode = targetPos

    OPEN.append(startnode)

    while len(OPEN) > 0:
        current = OPEN[0]
        while i < len(OPEN):
            i += 1
            if OPEN[i].fCost < current.fCost or OPEN[i].fCost == current.fCost and OPEN[i].hCost < current.hCost:
                current = OPEN[i]

        OPEN.remove(current)
        CLOSED.append(current)

        if current == endnode:
            return


FindPath()
