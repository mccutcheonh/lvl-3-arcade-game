import arcade

import constants
import math
import random


class PlayerCharacter(arcade.Sprite):
    """ The player class. """
    def __init__(self):
        super().__init__()
        # variables
        self.FACING = 0
        self.cur_texture = 0
        self.A = False
        self.D = False
        self.level = 1

        # all the animations for the player

        self.player_walkingr = []
        for i in range(4):
            texture = arcade.load_texture('player/Player Basic Walking.png', x=i * 32, y=0, width=32, height=58)
            self.player_walkingr.append(texture)
        self.player_walkingl = []
        for i in range(4):
            texture = arcade.load_texture('player/Player Basic Walking.png', x=i * 32, y=0, width=32, height=58,
                                          mirrored=True)
            self.player_walkingl.append(texture)
        self.walking = []
        self.walking.append(self.player_walkingr)
        self.walking.append(self.player_walkingl)

        self.player_idler = []
        for i in range(1):
            texture = arcade.load_texture('player/Player Basic Idle.png', x=i * 32, y=0, width=32, height=58)
            self.player_idler.append(texture)
        self.player_idlel = []
        for i in range(1):
            texture = arcade.load_texture('player/Player Basic Idle.png', x=i * 32, y=0, width=32, height=58,
                                          mirrored=True)
            self.player_idlel.append(texture)
        self.idle = []
        self.idle.append(self.player_idler)
        self.idle.append(self.player_idlel)
        self.texture = self.idle[self.FACING][0]
        self.scale = constants.PLAYER_SCALING



        # end of animations for the player

    def on_key_press(self, key: int):
        if key == arcade.key.SPACE or key == arcade.key.W:
            if self.physics_engines[self.level - 1].can_jump():
                self.change_y = constants.JUMP_SPEED
        elif key == arcade.key.A:
            self.A = True
        elif key == arcade.key.D:
            self.D = True

    def on_key_release(self, key: int):
        if key == arcade.key.A:
            self.A = False
        elif key == arcade.key.D:
            self.D = False

    def update(self):
        # Add some friction

        if self.change_x > constants.FRICTION:
            self.change_x -= constants.FRICTION
        elif self.change_x < -constants.FRICTION:
            self.change_x += constants.FRICTION
        else:
            self.change_x = 0

        # Apply acceleration based on the keys pressed

        if self.A and not self.D:
            self.change_x += -constants.ACCELERATION_RATE
        elif self.D and not self.A:
            self.change_x += constants.ACCELERATION_RATE

        if self.change_x > constants.MAX_SPEED:
            self.change_x = constants.MAX_SPEED
        elif self.change_x < -constants.MAX_SPEED:
            self.change_x = -constants.MAX_SPEED

    def update_animation(self, delta_time: float = 1 / 60):
        if self.change_x < 0 and self.FACING == 0 or self.change_x > 0 and self.FACING == 1:
            self.cur_texture += 1
            if self.cur_texture >= 4 * constants.UPDATES_PER_FRAME:
                self.cur_texture = 0
            self.texture = self.walking[self.FACING][-self.cur_texture // constants.UPDATES_PER_FRAME]
        elif self.change_x != 0:
            self.cur_texture += 1
            if self.cur_texture >= 4 * constants.UPDATES_PER_FRAME:
                self.cur_texture = 0
            self.texture = self.walking[self.FACING][self.cur_texture // constants.UPDATES_PER_FRAME]

        elif self.change_x == 0:
            self.texture = self.idle[self.FACING][0]




class PlayerBullet(arcade.Sprite):
    """ The players bullet. """
    def __init__(self):
        super().__init__()
        # Create a bullet
        self.texture = arcade.load_texture("player/Bullet.png")
        self.scale = constants.SPRITE_SCALING_LASER


class PlayerHands(arcade.Sprite):
    """ Players gun/arm"""
    def __init__(self):
        super().__init__()
        self.bullet_spread = 3
        self.magazine = 30
        self.reload = False
        self.FACING = 0
        self.cur_texture = 0
        self.shoot_cooldown = 0
        # list of True/False variables
        self.animate = False
        self.mouse_held = False
        # list of valiables to set
        self.bullet_list = None
        self.bullet = None
        self.wall_list = None
        self.aim_x = None
        self.aim_y = None
        self.player = None

        # the sprite sheets for the player's arms

        self.player_arms_facingr = []
        for i in range(1):
            texture = arcade.load_texture('player/Player Gun Idle.png', x=i * 60, y=0, width=60, height=32)
            self.player_arms_facingr.append(texture)

        self.player_arms_facingl = []
        for i in range(1):
            texture = arcade.load_texture('player/Player Gun Idle.png', x=i * 60, y=0, width=60, height=32,
                                          flipped_vertically=True)
            self.player_arms_facingl.append(texture)
        self.player_arms = []
        self.player_arms.append(self.player_arms_facingr)
        self.player_arms.append(self.player_arms_facingl)
        self.texture = self.player_arms[self.FACING][0]

        self.player_reloadingr = []
        for i in range(14):
            texture = arcade.load_texture('player/Player Gun Reloading.png', x=i * 60, y=0, width=60, height=32)
            self.player_reloadingr.append(texture)
        self.player_reloadingl = []
        for i in range(14):
            texture = arcade.load_texture('player/Player Gun Reloading.png', x=i * 60, y=0, width=60, height=32,
                                          flipped_vertically=True)
            self.player_reloadingl.append(texture)
        self.player_shooting = []
        self.player_shooting.append(self.player_reloadingr)
        self.player_shooting.append(self.player_reloadingl)

        self.scale = constants.SCALING




        self.setup()

    def setup(self):

        self.bullet_list = arcade.SpriteList()
        self.player = PlayerCharacter()

    def on_key_press(self, key: int):
        if key == arcade.key.R and self.magazine < 30:
            self.reload = True

    def on_mouse_press(self):
        self.mouse_held = True

    def on_mouse_release(self):
        self.mouse_held = False
        self.bullet_spread = 3


    def shoot(self):
        if self.magazine >> 0:
            self.magazine -= 1
            bullet = PlayerBullet()
            # Position the bullet at the player's current location
            start_x = self.center_x
            start_y = self.center_y
            bullet.center_x = start_x
            bullet.center_y = start_y

            # Get from the mouse the destination location for the bullet
            dest_x = self.aim_x
            dest_y = self.aim_y

            # Do math to calculate how to get thdae bullet to the destination.
            # Calculation the angle in radians between the start points
            # and end points. This is the angle the bullet will travel.
            x_diff = dest_x - start_x
            y_diff = dest_y - start_y
            angle = math.atan2(y_diff, x_diff)

            # Angle the bullet sprite so it doesn't look like it is flying
            # sideways.
            bullet.angle = math.degrees(angle)
            print(f"Bullet angle: {bullet.angle:.2f}")

            # Taking into account the angle, calculate our change_x
            # and change_y. Velocity is how fast the bullet travels.
            self.bullet_spread += .5
            bullet.change_x = math.cos(angle) * constants.BULLET_SPEED + random.randrange(1, int(self.bullet_spread))
            bullet.change_y = math.sin(angle) * constants.BULLET_SPEED + random.randrange(1, int(self.bullet_spread))

            self.bullet_list.append(bullet)

    def on_draw(self):
        self.bullet_list.draw()

    def update(self):
        if self.mouse_held:
            if self.shoot_cooldown % 5 == 0:
                self.shoot()
            self.shoot_cooldown += 1
        elif not self.mouse_held and self.shoot_cooldown % 5 != 0:
            self.shoot_cooldown += 1

        self.bullet_list.update()

        for bullet in self.bullet_list:

            # Check this bullet to see if it hit a coin

            hit_list = arcade.check_for_collision_with_list(bullet, self.wall_list)

            # If it did, get rid of the bullet
            if len(hit_list) > 0 or arcade.check_for_collision(bullet, self.player):
                bullet.remove_from_sprite_lists()
                print('hit wall or player')

            # If the bullet flies off-screen, remove it.
            if bullet.bottom > constants.SCREEN_HEIGHT or bullet.top < 0:
                bullet.remove_from_sprite_lists()
                print('went off screen')

    def update_animation(self, delta_time: float = 1 / 60):
        # Position the start at the enemy's current location
        start_x = self.center_x
        start_y = self.center_y

        # Get the destination location for the bullet
        dest_x = self.aim_x
        dest_y = self.aim_y

        x_diff = dest_x - start_x
        y_diff = dest_y - start_y
        angle = math.atan2(y_diff, x_diff)

        # Set the enemy to face the player.
        self.angle = math.degrees(angle)

        if 1.6 >= angle >= -1.6:
            self.FACING = 0
            self.player.FACING = 0

        else:
            self.FACING = 1
            self.player.FACING = 1

        if self.reload and not self.mouse_held:
            self.texture = self.player_shooting[self.FACING][self.cur_texture // constants.UPDATES_PER_FRAME]
            self.cur_texture += 1
            if self.cur_texture >= 14 * constants.UPDATES_PER_FRAME:
                self.cur_texture = 0
                self.reload = False
                self.magazine = 30
        else:
            self.texture = self.player_arms[self.FACING][0]